var Bicicleta = function(id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return "id: " + this.id + " | modelo: " + this.modelo; 
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function (aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici){
        return aBici;
    }else{
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
    }
}

Bicicleta.removeById = function (aBiciId) {
    Bicicleta.findById(aBiciId);
    for (let index = 0; index < Bicicleta.allBicis.length; index++) {
        if (Bicicleta.allBicis[index].id == aBiciId){
            Bicicleta.allBicis.splice(index,1);
            break;
        }
        
    }
}

var a =  new Bicicleta(1,"Roja","Urbana",[19.2967599,-98.8898438]);
var b =  new Bicicleta(2,"Azul","BMX",[19.3017573,-98.8649891]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;